fajl = mfilename('fullpath');
folder = fajl(1:length(fajl)-9);

prompt1 = 'Input file?\n';
inName = input(prompt1, 's');
prompt2 = 'Output file?\n';

outName = strcat(folder, input(prompt2, 's'));
%'C:\\Users\\student\\Favorites\\Desktop\\prm\\asembler.txt'
%'C:\\Users\\student\\Favorites\\Desktop\\prm\\izlaz.txt'

idin = fopen(inName, 'r');
idout = fopen(outName, 'wt');

pocetakFajla(idout);

cnt=0;

while ~feof(idin)
    str = fgetl(idin);

    lines = regexp(str, '\ ', 'split');
    h = size(lines,2);

    if( h < 3 ) 
        fprintf(idout, 'Greska u kodu!\n');
        krajFajla(idin, idout);
    end

    switch lines{3}
        case 'JMP'
           if( h~=4 )
               greskica(lines{1}, idin, idout);
           end
           fprintf(idout, '%s : 0000;\n', lines{1});
           
           rec(lines{1}, lines{4},idin,idout);
           
        case 'JSR'
           if( h~=4 )
                greskica(lines{1}, idin, idout);
           end
                fprintf(idout, '%s : 0100;\n', lines{1});
                
                rec(lines{1}, lines{4},idin,idout);
                
        case 'BEQL'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 4000;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            
        case 'BNEQL'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 4100;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            
        case 'BNEG'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 4200;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            
        case 'BNNEG'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 4300;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            
        case 'BOVF'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 4400;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            
        case 'BNOVF'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 4500;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            
        case 'BCAR'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 4600;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            
        case 'BNCAR'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 4700;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            
        case 'BGRT'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 4800;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            
        case 'BGRTE'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 4900;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            
        case 'BLSS'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 4A00;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            
        case 'BLSSE'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 4B00;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            
        case 'BGRTU'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 4C00;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            
        case 'BGRTEU'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 4D00;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            
        case 'BLSSU'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 4E00;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            
        case 'BLSSEU' 
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 4F00;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            

        case 'PUSH'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : C000;\n', lines{1});
            
        case 'POP'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : C100;\n', lines{1});
            
        case 'INC'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : C200;\n', lines{1});
            
        case 'DEC'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : C300;\n', lines{1});
            
        case 'ASR'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : C400;\n', lines{1});
            
        case 'LSR'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : C500;\n', lines{1});
            
        case 'ROR'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : C600;\n', lines{1});
            
        case 'RORC'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : C700;\n', lines{1});
            
        case 'ASL'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : C800;\n', lines{1});
            
        case 'LSL'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : C900;\n', lines{1});
            
        case 'ROL'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : CA00;\n', lines{1});
            
        case 'ROLC'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : CB00;\n', lines{1});
            
        case 'INTE'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : CC00;\n', lines{1});
            
        case 'INTD'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : CD00;\n', lines{1});
            
        case 'RTI'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : CE00;\n', lines{1});
            
        case 'RTS'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : CF00;\n', lines{1});
            


        case 'INT'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : D000;\n', lines{1});
            
            rec(lines{1}, lines{4},idin,idout);
            
        case 'HALT'
            if( h~=3 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : D100;\n', lines{1});
            

        case 'LD'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 8', lines{1});
            
            adresiranje(lines{1}, 0, lines{4}, idin, idout);
        case 'ST'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 8', lines{1});
            
            adresiranje(lines{1}, 1, lines{4}, idin, idout);
        case 'ADD'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 9', lines{1});
            
            adresiranje(lines{1}, 0, lines{4}, idin, idout);
        case 'SUB'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : 9', lines{1});
            
            adresiranje(lines{1}, 1, lines{4}, idin, idout);
        case 'AND'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : A', lines{1});
            adresiranje(lines{1}, 0, lines{4}, idin, idout);
        case 'OR'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : A', lines{1}); 
            adresiranje(lines{1}, 1, lines{4}, idin, idout);
        case 'XOR'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : B', lines{1});
            adresiranje(lines{1}, 0, lines{4}, idin, idout);
        case 'NOT'
            if( h~=4 )
                greskica(lines{1}, idin, idout);
            end
            fprintf(idout, '%s : B', lines{1});
            adresiranje(lines{1}, 1, lines{4}, idin, idout);
        otherwise
            greskica(lines{1}, idin, idout);
    end
end


krajFajla(idin, idout);
