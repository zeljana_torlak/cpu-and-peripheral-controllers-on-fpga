function rec(PC, word,idin,idout)
    PCnext = num2str(dec2hex(hex2dec(PC) + 1));
    if( size(word, 1) > 4 )
        greskica(PCnext, idin, idout);
    end
    for i=1:size(word, 1)
        if ~( (word(i) >= '0' && word(i) <='9') || (word(i) >= 'A' && word(i) <='F') || (word(i) >= 'a' && word(i) <='f'))
            greskica(PCnext, idin, idout);
        end
    end
    fprintf(idout, '%s : %s;\n', PCnext, word);
end