function  adresiranje(PC, fleg, word, idin, idout)

    % immed #XXXX
    % memdir XXXX
    % memind [XXXX]
    % regdir RXX
    % regind [RXX]
    % else GRADR
    
    PCnext = num2str(dec2hex(hex2dec(PC) + 1));
    switch word(1)
        case '#'
            %immed
            if (fleg == 1)
                fprintf(idout, 'C00;\n');
            else
                fprintf(idout, '400;\n');
            end
            %disp(word(2:length(word)));
            fprintf(idout, '%s : %s;\n', PCnext, word(2:length(word)));
        case '['
            if( word(2) == 'R' ) 
                %regind
                if (fleg == 1)
                    fprintf(idout, '9');
                else
                    fprintf(idout, '1');
                end
                broj = str2double(word(3:(length(word)-1)));
                if( broj <= 15 )
                    fprintf(idout, '0%x;\n', broj);
                else
                    fprintf(idout, '%x;\n', broj);
                end
            else
                %memind
                if (fleg == 1)
                    fprintf(idout, 'B00;\n');
                else
                    fprintf(idout, '300;\n');
                end
                fprintf(idout, '%s : %s;\n', PCnext, word(2:(length(word)-1)));
            end
        case 'R'
            %regdir
            if (fleg == 1)
                fprintf(idout, '8');
            else
                fprintf(idout, '0');
            end
            broj = str2double(word(2:length(word)));
                if( broj <= 15 )
                    fprintf(idout, '0%x;\n', broj);
                else
                    fprintf(idout, '%x;\n', broj);
                end
        otherwise
            if( (word(1) >= '0' && word(1) <='9') || (word(1) >= 'A' && word(1) <='F') || (word(1) >= 'a' && word(1) <='f')) 
                %memdir
                if (fleg == 1)
                    fprintf(idout, 'A00;\n');
                else
                    fprintf(idout, '200;\n');
                end
                fprintf(idout, '%s : %s;\n', PCnext, word);
            else
                %gradr
                 fprintf(idout, 'F00;\n');
                 disp('GRADR!!!');
                 krajFajla(idin, idout);
            end
    end
end