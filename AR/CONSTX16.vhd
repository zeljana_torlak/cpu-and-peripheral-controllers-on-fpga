library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

entity CONSTX16 is

	generic
	(
		size  : natural  := 16;
		const : integer  :=0
	);

	port 
	(
		data: out std_logic_vector(size-1 downto 0)
	);

end entity;

architecture rtl of CONSTX16 is
begin
	data<=conv_std_logic_vector(const, size);
end rtl;
